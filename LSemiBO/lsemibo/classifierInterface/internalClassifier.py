from .classifierInterface import ClassifierStructure
from sklearn.svm import SVC
import numpy as np

class InternalClassifier(ClassifierStructure):
    def __init__(self):
        self.model = SVC(C=1.0, kernel='rbf', degree=6)

    def fit_classifier(self, x_train, y_train):
        self.model.fit(x_train, y_train)

    def predict_classifier(self, x_test):
        y_test = self.model.predict(x_test)        
        return y_test