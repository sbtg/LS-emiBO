from ..gprInterface import GPR
from ..sampling import uniform_sampling, lhs_sampling
from scipy.stats import norm
from scipy import stats

from copy import deepcopy
import numpy as np

class SpecEI:
    def __init__(self, identifier, x_train, y_train, best_point, mapping_indices, gpr_model, region_support, R, M, tf_dim, rng, sampling_type = "lhs_sampling"):
        self.id = identifier
        self.x_train = x_train
        self.y_train = y_train
        self.mapping_indices = mapping_indices
        self.sampling_type = sampling_type
        self.region_support = region_support
        self.tf_dim = tf_dim
        self.rng = rng
        self.R = R
        self.M = M
        self.model = deepcopy(GPR(gpr_model))
        self.model.fit(self.x_train, self.y_train)
        self.best_point = best_point
        # self.prob = [self._cal_prob() for _ in range(self.R)]
        self.prob = self._cal_prob()
        
    def _cal_prob(self):
        
        cdf_all_sum = 0
        
        for _ in range(self.R):
            if self.sampling_type == "lhs_sampling":
                samples = lhs_sampling(self.M, self.region_support, self.tf_dim, self.rng)
            elif self.sampling_type == "uniform_sampling":
                samples = uniform_sampling(self.M, self.region_support, self.tf_dim, self.rng)
            else:
                raise ValueError(f"{self.sampling_type} not defined. Currently only Latin Hypercube Sampling and Uniform Sampling is supported.")
            samples_subset = samples[:, self.mapping_indices]
            y_pred, pred_sigma = self.model.predict(samples_subset)

            cdf_all_sum += np.sum(stats.norm.cdf(self.best_point, y_pred, pred_sigma))
            
        return (cdf_all_sum)
    


    def _surrogate(self, x_train):
        """_surrogate Model function

        Args:
            model: Gaussian process model
            X: Input points

        Returns:
            [type]: predicted values of points using gaussian process model
        """

        return self.model.predict(x_train)

    def _acquisition(self, sample, sample_type="single"):
        
        if len(sample.shape) == 1:
            sample = sample.reshape((-1,1)).T


        sample_subset = sample[:, self.mapping_indices]
        curr_best = self.best_point
        
        if sample_type == "multiple":
            
            mu, std = self._surrogate(sample_subset)
            ei_list = []
            for mu_iter, std_iter in zip(mu, std):
                pred_var = std_iter
                
                if pred_var > 0:
                    var_1 = curr_best - mu_iter
                    var_2 = var_1 / pred_var

                    ei = (var_1 * norm.cdf(var_2)) + (
                        pred_var * norm.pdf(var_2)
                    )
                else:
                    ei = 0.0

                ei_list.append(ei)

            return np.array(ei_list)

        elif sample_type == "single":
            mu, std = self._surrogate(sample_subset.reshape(1, -1))
            
            pred_var = std[0]
            if pred_var > 0:
                var_1 = curr_best - mu[0]
                var_2 = var_1 / pred_var

                ei = (var_1 * norm.cdf(var_2)) + (
                    pred_var * norm.pdf(var_2)
                )
            else:
                ei = 0.0
            return ei