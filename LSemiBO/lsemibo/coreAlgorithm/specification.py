# Complex coupling of Requirements, sub components and compute_robustness

# class Requirements:
#     1) holds sub specs, mapping, _call_ to compute robustness
#     2) What specs are active
#     3) holds y_train for components
#     4) should be able to give out emi-EI
#     5) classification inside or outside?

import numpy as np
from copy import deepcopy
from staliro.specifications import RTAMTDense
from scipy.stats import norm
from scipy import stats
from scipy.optimize import minimize
# from pyswarms.single.global_best import GlobalBestPSO
# import time

from ..classifierInterface import Classifier
from ..sampling import lhs_sampling, uniform_sampling
from ..utils import sample_spec
from ..gprInterface import GPR
from .specGPR import SpecEI

class Component:
    def __init__(self, identifier, spec, pred_mapping) -> None:
        self.id = identifier
        self.spec = spec
        self.pred_mapping = pred_mapping
        self.specification = RTAMTDense(self.spec, self.pred_mapping)
        self.count = 0

    def __call__(self, traj):
        self.count += 1
        robustness = self.specification.evaluate(traj.states, traj.times)

        return robustness

    


class SingleRequirement:
    def __init__(self, tf_dim, component_list, predicate_mapping, seed = 12344) -> None:
        self.requirement_list = []
        self.num_components = len(component_list)
        self.io_mapping = []

        for iter, spec in enumerate(component_list):
            predicate_mapping_local = {}
            input_indices_component = []
            for var in predicate_mapping.keys():
                if var in spec:
                    index, predicate_mapping_local[var] = predicate_mapping[var]
                    if index not in input_indices_component:
                        input_indices_component += index
                    # input_indices.update(index)
            mapping = np.array([1 if item in input_indices_component else 0 for item in range(tf_dim)])
            
            self.io_mapping.append(mapping)
            
            self.requirement_list.append(Component(iter, spec, predicate_mapping_local))
        
        self.count = 0
        self.y_train_component = np.empty((0, self.num_components))
        self.requirement_falsified = False
        self.rng = np.random.default_rng(seed)

    def evaluate(self, traj):
        component_rob = []
        
        for iterate in range(self.num_components):
            result = self.requirement_list[iterate](traj)
            component_rob.append(result)
            
        
        requirement_rob = min(component_rob)
        self.y_train_component = np.vstack([self.y_train_component, np.array([component_rob])])

        if requirement_rob <= 0 and (not self.requirement_falsified):
            self.requirement_falsified = True
        
        self.count += 1
        return requirement_rob, self.requirement_falsified

    def lsemibo_req(self, top_k, x_train, y_train, gpr_model, classifier_model, cs_budget, classified_sample_bias, R, M, region_support, tf_dim, cs_sampling_type, pi_sampling_type):
        
        sampled_specs = self.choose_top_k(top_k, x_train, classifier_model, cs_budget, region_support, tf_dim, cs_sampling_type, classified_sample_bias)
        # print(f"Sampled Specs = {sampled_specs}")
        component_ei = []
        best_point = np.min(y_train)
        pi = []
        # print(f"Best Point until now = {best_point}")
        for iterate in range(len(sampled_specs)):
            
            mapping_indices = np.where(self.io_mapping[sampled_specs[iterate]] == 1)[0]
            # Subspace of multidimensional 
            # Rename variables
            x_train_subset = x_train[:, mapping_indices]
            y_train_subset = self.y_train_component[:, sampled_specs[iterate]]
            
            
            req_comp = SpecEI(iterate, x_train_subset, y_train_subset, best_point, mapping_indices, gpr_model, region_support, R, M, tf_dim, self.rng, pi_sampling_type)
            
            pi.append(req_comp.prob)
            component_ei.append(req_comp)
        
        
        pi_mc = np.array(pi) / (np.sum(pi))
        # print(f"Original PI = {pi}\nNormalized Value and Sum = {pi_mc}, {np.sum(pi_mc)}")
        pred_sample_x = self._opt_acquisition(component_ei, pi_mc, region_support, tf_dim)
        return pred_sample_x

    def choose_top_k(self, top_k, x_train, classifier_model, cs_budget, region_support, tf_dim, sampling_type, classified_sample_bias):
        top_k = min(top_k, self.num_components)
        if self.num_components == 1:
            sampled_specs = [0]
        else:
            classifier_model = Classifier(classifier_model)
            y_train_comp_min_classi = np.argmin(self.y_train_component,1)
            classifier_model.fit(x_train, y_train_comp_min_classi)
            
            if sampling_type == "lhs_sampling":
                x_test = lhs_sampling(cs_budget, region_support, tf_dim, self.rng)
                # x_test_classifier_active = x_test 
            elif sampling_type == "uniform_sampling":
                x_test = uniform_sampling(cs_budget, region_support, tf_dim, self.rng)
                # x_test_classifier_active = x_test 
            else:
                raise ValueError(f"{sampling_type} not defined. Currently only Latin Hypercube Sampling and Uniform Sampling is supported.")

            y_test_classifier = classifier_model.predict(x_test)

            unique, counts = np.unique(y_test_classifier, return_counts=True)
            spec_prob = {}
            classified_spec_prob = []
            unclassified_spec_prob = []
            for spec_number in range(self.num_components):
                if spec_number not in set(unique):
                    spec_prob[spec_number] = 0
                    unclassified_spec_prob.append(spec_number)
                else:
                    spec_prob[spec_number] = counts[np.where(unique == spec_number)][0] / cs_budget
                    classified_spec_prob.append(spec_number)
            sampled_specs = sample_spec(spec_prob, unclassified_spec_prob, classified_spec_prob, top_k, classified_sample_bias, self.rng)

        return sampled_specs

    
    def _opt_acquisition(self, spec_ei, pi_mc, region_support, tf_dim):
        """Get the sample points

        Args:
            X (np.array): sample points
            y (np.array): corresponding robustness values
            model ([type]): the GP models
            sbo (list): sample points to construct the robustness values
            test_function_dimension (int): The dimensionality of the region. (Dimensionality of the test function)
            region_support (np.array): The bounds of the region within which the sampling is to be done.
                                        Region Bounds is M x N x O where;
                                            M = number of regions;
                                            N = test_function_dimension (Dimensionality of the test function);
                                            O = Lower and Upper bound. Should be of length 2;

        Returns:
            [np.array]: the new sample points by BO
            [np.array]: sbo - new samples for resuse
        """

        lower_bound_theta = np.ndarray.flatten(region_support[:, 0])
        upper_bound_theta = np.ndarray.flatten(region_support[:, 1])
        # bounds = (lower_bound_theta, upper_bound_theta)

        # bnds = Bounds(lower_bound_theta, upper_bound_theta)
        fun = lambda _x: -1 * self._emi_ei(_x, spec_ei, pi_mc)
        # t = time.time()
        random_samples = uniform_sampling(10000, region_support, tf_dim, self.rng)
        min_bo_val = -1 * self._emi_ei(
            random_samples, spec_ei, pi_mc, sample_type="multiple"
        )


        
        min_bo = np.array([random_samples[np.argmin(min_bo_val), :]])
        min_bo_val = np.min(min_bo_val)

        
        for _ in range(30):
            new_params = minimize(
                fun,
                bounds=list(zip(lower_bound_theta, upper_bound_theta)),
                method = "L-BFGS-B",
                x0=min_bo,
            )
            # print(new_params)

            if not new_params.success:
                continue

            if min_bo is None or fun(new_params.x) < min_bo_val:
                min_bo = new_params.x
                min_bo_val = fun(min_bo)

        new_params = minimize(
            fun, bounds=list(zip(lower_bound_theta, upper_bound_theta)), x0=min_bo
        )
        
        min_bo = new_params.x
        # local_minimier_time = time.time() - t
        
        # t = time.time()
        # options = {'c1':0.5, 'c2': 0.3, 'w':0.9, 'k':2, 'p':2}
        # # bnds = Bounds(lower_bound_theta, upper_bound_theta)
        # fun = lambda _x: -1 * self._emi_ei(_x, spec_ei, pi_mc)
        # optimizer = GlobalBestPSO(n_particles = 200, dimensions=tf_dim, options=options, bounds=bounds)
        # cost, pos = optimizer.optimize(fun, iters=50)
        # pso_time = time.time() - t
        # print("********************")
        # print("********************")
        # print("********************")

        # print(f"Time Taken for restart = {local_minimier_time}")
        # print(min_bo)
        # print(self._emi_ei(np.array(min_bo), spec_ei, pi_mc))
        # print("***************************")
        # print(f"Time Taken for PSO = {pso_time}")
        # print(pos)
        # print(cost)
        
        # print("********************")
        # print("********************")
        # print("********************")
        # print(pos)
        # print(vdfavd)
        return np.array(min_bo)

    def _emi_ei(self, x, spec_ei, pi_mc, sample_type="single"):
        if x.shape[0] != 1:
            sample_type = "multiple"
        prob = []
        comp_ei = []
        
        for specs in spec_ei:
            comp_ei.append(specs._acquisition(x, sample_type))
        
        prob = np.array(pi_mc)
        if sample_type == "single":
            comp_ei = np.array([comp_ei])
        elif sample_type == "multiple":
            comp_ei = np.array(comp_ei).T
        
        # print("*********")
        # print(prob)
        # print(comp_ei)
        # print("*********")
        ei = np.sum(np.array(comp_ei) * prob, 1)
        
        return ei
