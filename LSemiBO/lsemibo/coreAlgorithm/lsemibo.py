from .specification import SingleRequirement
from ..sampling import uniform_sampling, lhs_sampling
from ..utils import compute_robustness, Evaluator

from staliro.staliro import _signal_bounds
import numpy as np
from tqdm import tqdm
import pathlib
import pickle
import time

class LSemiBO:
    def __init__(self, benchmark_name, run_number, is_budget, max_budget, cs_budget, top_k, classified_sample_bias, model, component_list, predicate_mapping, tf_dim, options, R, M, is_type = "lhs_sampling", cs_type = "lhs_sampling", pi_type = "lhs_sampling", seed = 12345):
        self.run_number = run_number
        self.benchmark_name = benchmark_name
        base_path = pathlib.Path()
        self.result_directory = base_path.joinpath(benchmark_name)
        self.result_directory.mkdir(exist_ok=True)

        self.bo_budget = max_budget - is_budget
        signal_bounds = _signal_bounds(options.signals)
        bounds = options.static_parameters + signal_bounds
        self.region_support = np.array((tuple(bound.astuple() for bound in bounds),))[0]
        self.rng = np.random.default_rng(seed)
        self.is_type = is_type
        self.is_budget = is_budget
        self.cs_budget = cs_budget
        self.cs_type = cs_type
        self.max_budget = max_budget
        self.rem_budget = self.max_budget - self.is_budget
        self.tf_dim = tf_dim
        self.R = R
        self.M = M
        self.num_requirements = len(component_list)
        self.tf_wrapper = Evaluator(model, options, self.num_requirements)
        self.top_k = top_k
        self.classified_sample_bias = classified_sample_bias
        self.requirements = []
        self.pi_type = pi_type

        for iterate in range(self.num_requirements):
            self.requirements.append(SingleRequirement(tf_dim, component_list[iterate], predicate_mapping))

    def _select_requirement(self, curr_active_specs):
        if sum(curr_active_specs) != 0:
            return 0
        
    def sample(self, input_gpr_model, input_classifier_model):
        start_time = time.perf_counter()

        if self.is_type == "lhs_sampling":
            x_train = lhs_sampling(self.is_budget, self.region_support, self.tf_dim, self.rng)
        elif self.is_type == "uniform_sampling":
            x_train = uniform_sampling(self.is_budget, self.region_support, self.tf_dim, self.rng)
        else:
            raise ValueError(f"{self.is_type} not defined. Currently only Latin Hypercube Sampling and Uniform Sampling is supported.")

        y_train, curr_active_specs = compute_robustness(x_train, self.requirements, self.tf_wrapper)
        
        for budget in tqdm(range(self.max_budget - self.tf_wrapper.count)):
            # print(f"***************************************************")
            # print(f"********************{budget}***********************")
            
            if np.sum(curr_active_specs) <= self.num_requirements:
                if np.sum(curr_active_specs) == 0:
                    print("All reqs falsified")
                    elapsed_time = time.perf_counter() - start_time
                    sampled_data = (x_train, y_train, elapsed_time)
                    with open(self.result_directory.joinpath(self.benchmark_name + f"_point_history_run{self.run_number}.pkl"), "wb") as f:
                        pickle.dump(sampled_data, f)
                    return (x_train, y_train, elapsed_time)
                else:
                    curr_req = self.requirements[self._select_requirement(curr_active_specs)]
                    
                    
                    pred_sample_x = curr_req.lsemibo_req(self.top_k, x_train, y_train, input_gpr_model, input_classifier_model, self.cs_budget, self.classified_sample_bias, self.R, self.M, self.region_support, self.tf_dim, self.cs_type, self.pi_type)
                    
                    x_train = np.vstack((x_train, np.array([pred_sample_x])))
                    # print(f"Sampled point = {pred_sample_x}")
                    pred_sample_y, curr_active_specs = compute_robustness(np.array([pred_sample_x]), self.requirements, self.tf_wrapper)
                    # print(f"Corresponding Robustness = {pred_sample_y}")
                    y_train = np.vstack((y_train, pred_sample_y))
        
        elapsed_time = time.perf_counter() - start_time
        sampled_data = (x_train, y_train, elapsed_time)
        with open(self.result_directory.joinpath(self.benchmark_name + f"_point_history{self.run_number}.pkl"), "wb") as f:
            pickle.dump(sampled_data, f)

        return (x_train, y_train, elapsed_time)