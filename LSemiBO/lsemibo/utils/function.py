from staliro.staliro import  simulate_model
# TODO
# Can the dimensionality of the training set be reduced?



class Evaluator:
    def __init__(self, model, options, num_requirements):
        self.model = model
        self.options = options
        self.num_requirements = num_requirements
        self.count = 0
        self.point_history = []
        

    def __call__(self, sample, requirements):
        self.count = self.count + 1
        
        result = simulate_model(self.model, self.options, sample)
        robustness = []
        curr_active_reqs = []
        
        for iterate in range(self.num_requirements):
            rob, falsified = requirements[iterate].evaluate(result)
            robustness.append(rob)
            if falsified:
                curr_active_reqs.append(0)
            else:
                curr_active_reqs.append(1)


        self.point_history.append([self.count, sample, robustness, curr_active_reqs])
        return robustness, curr_active_reqs
