import numpy as np
import numpy.typing as npt
from typing import List, Type
from .function import Evaluator


def compute_robustness(samples_in: npt.NDArray, requirements, test_function: Type[Evaluator]) -> npt.NDArray:
    """Compute the fitness (robustness) of the given sample.

    Args:
        samples_in: Samples points for which the fitness is to be computed.
        test_function: Test Function insitialized with Fn
    Returns:
        Fitness (robustness) of the given sample(s)
    """

    if samples_in.shape[0] == 1:
        samples_out, active_specs = test_function(samples_in[0], requirements)
        samples_out = np.array([samples_out])
        active_specs = np.array([active_specs])
    else:
        samples_out = []
        active_specs = []
        for sample in samples_in:
            samp_out, act_spe = test_function(sample, requirements)
            samples_out.append(samp_out)
            active_specs.append(act_spe)
        samples_out = np.array(samples_out)
        active_specs = np.array(active_specs)

    return samples_out, active_specs[-1,:]