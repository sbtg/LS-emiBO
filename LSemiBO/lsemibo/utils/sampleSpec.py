import numpy as np

def _sample(cumsum_dist_list, rng):
    intervals = []
    for iterate in range(len(cumsum_dist_list)-1):
        intervals.append([cumsum_dist_list[iterate], cumsum_dist_list[iterate+1]])

    intervals = np.array(intervals)
    
    coin_toss_2 = rng.random()
    index = np.where(np.logical_and(coin_toss_2 >= intervals[:,0], coin_toss_2 < intervals[:,1]) == True)[0][0]
    return index

def sample_spec(spec_prob, unclassified_spec, classified_spec, top_k, classified_sample_bias, rng):
    if top_k > len(classified_spec) + len(unclassified_spec):
        raise Exception("Top-K is gretaer than number of spcs present. Re-check implementation")
    elif top_k == len(classified_spec) + len(unclassified_spec):
        sampled_specs = unclassified_spec + classified_spec
    else:
        sampled_specs = []

        while len(sampled_specs) != top_k:
            
            if not unclassified_spec:
                # print("Case 1")
                prob = [spec_prob[item] for item in classified_spec]
                prob = [item / sum(prob) for item in prob]
                cumsum_dist_list_cl = np.insert(np.cumsum(prob),0,0)
                index = _sample(cumsum_dist_list_cl, rng)
                sampled_specs.append(classified_spec.pop(index))
            elif not classified_spec:
                # print("Case 1")
                interval_unclassified = len(unclassified_spec)
                cumsum_dist_list_uncl = np.insert(np.cumsum([1/interval_unclassified]*interval_unclassified),0,0)
                index = _sample(cumsum_dist_list_uncl, rng)
                sampled_specs.append(unclassified_spec.pop(index))
            else:

                coin_toss = rng.random()
                if (coin_toss < classified_sample_bias):
                    # print("Case 3.1")
                    prob = [spec_prob[item] for item in classified_spec]
                    prob = [item / sum(prob) for item in prob]
                    cumsum_dist_list_cl = np.insert(np.cumsum(prob),0,0)
                    index = _sample(cumsum_dist_list_cl, rng)
                    sampled_specs.append(classified_spec.pop(index))
                else:
                    # print("Case 3.2")
                    interval_unclassified = len(unclassified_spec)
                    cumsum_dist_list_uncl = np.insert(np.cumsum([1/interval_unclassified]*interval_unclassified),0,0)
                    index = _sample(cumsum_dist_list_uncl, rng)
                    sampled_specs.append(unclassified_spec.pop(index))
            # print(sampled_specs)
            # print(classified_spec)
            # print(unclassified_spec)
            # print("**************")
            # print("**************")
            # print("**************")
    return sampled_specs

